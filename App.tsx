import React from 'react';

import Walkthrough from './src/screens/walkthrough/Walkthrough';
import {SafeAreaView} from 'react-native';

import LogoSvg from './assets/logo.svg';

function App(): JSX.Element {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#000'}}>
      <Walkthrough />
      <LogoSvg />
    </SafeAreaView>
  );
}

export default App;
