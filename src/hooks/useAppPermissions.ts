import {Alert, Linking, Platform} from 'react-native';
import {PERMISSIONS, RESULTS, request} from 'react-native-permissions';

const useAppPermissions = () => {
  const requestCameraPermission = async () => {
    try {
      const permissionStatus = await request(
        Platform.OS === 'ios'
          ? PERMISSIONS.IOS.CAMERA
          : PERMISSIONS.ANDROID.CAMERA,
      );
      return permissionStatus;
    } catch (error) {
      console.error('Error requesting camera permission:', error);
      return RESULTS.DENIED;
    }
  };
  const requestStoragePermissions = async () => {
    if (Platform.OS === 'android') {
      const readStatus = await request(
        PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
      );
      const writeStatus = await request(
        PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
      );
      console.log(readStatus, writeStatus);

      if (readStatus === 'blocked' || writeStatus === 'blocked') {
        showPermissionExplanation();
      }
    }
    // iOS doesn't require explicit storage permissions
  };
  const showPermissionExplanation = () => {
    Alert.alert(
      'Permission Required',
      'This app requires access to your device storage to function properly. Please grant the required permissions in your device settings.',
      [
        {
          text: 'Open Settings',
          onPress: () => Linking.openSettings(),
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Permission denied'),
        },
      ],
    );
  };
  // iOS doesn't require explicit storage permissions

  return {requestCameraPermission, requestStoragePermissions};
};

export default useAppPermissions;
