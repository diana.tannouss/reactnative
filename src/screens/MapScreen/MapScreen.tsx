import React, {useRef} from 'react';
import MapView, {MapViewProps} from 'react-native-maps';

const MapComponent = React.forwardRef<MapView, MapViewProps>((props, ref) => {
  return (
    <MapView
      ref={ref}
      style={{flex: 1}}
      initialRegion={{
        latitude: 33.8547,
        longitude: 35.8623,
        latitudeDelta: 1.5,
        longitudeDelta: 1.5,
      }}
    />
  );
});

const MapScreen = () => {
  const mapRef = useRef<MapView>(null);
  return <MapComponent ref={mapRef} />;
};

export default MapScreen;
